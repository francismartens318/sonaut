package com.idalko.sonar;

import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.service.client.CrowdClient;
import org.sonar.api.security.ExternalUsersProvider;
import org.sonar.api.security.UserDetails;

/**
 * @author Hein Couwet
 */
public class CrowdUsersProvider extends ExternalUsersProvider {
    private CrowdClient crowdClient;

    public CrowdUsersProvider(CrowdClient crowdClient) {
        this.crowdClient = crowdClient;
        if (crowdClient == null) {
            throw new CrowdRuntimeException("crowdClient is null");
        }
    }

    @Override
    public UserDetails doGetUserDetails(String username) {
        try {
            User user = crowdClient.getUser(username);
            UserDetails result = new UserDetails();
            result.setName(user.getDisplayName());
            result.setEmail(user.getEmailAddress());
            return result;
        } catch (UserNotFoundException e) {
            throw new CrowdRuntimeException("User ["+username+"] Not Found",e);
        } catch (OperationFailedException e) {
            throw new CrowdRuntimeException("Operation Failed Exception",e);
        } catch (ApplicationPermissionException e) {
            throw new CrowdRuntimeException("Application Permission Exception",e);
        } catch (InvalidAuthenticationException e) {
            throw new CrowdRuntimeException("Invalid Authentication",e);
        }
    }
}
