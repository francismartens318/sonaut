package com.idalko.sonar;

import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.service.client.CrowdClient;
import org.sonar.api.security.ExternalGroupsProvider;

import java.util.Collection;

/**
 * @author Hein Couwet
 */
public class CrowdGroupsProvider extends ExternalGroupsProvider {
    public static final int MAX_GROUPS = 512;

    private CrowdClient crowdClient;

    public CrowdGroupsProvider(CrowdClient crowdClient) {
        this.crowdClient = crowdClient;
        if (crowdClient == null) {
            throw new CrowdRuntimeException("crowdClient is null");
        }
    }

    @Override
    public Collection<String> doGetGroups(String username) {
        try {
            return crowdClient.getNamesOfGroupsForNestedUser(username,0,MAX_GROUPS);
        } catch (UserNotFoundException e) {
            throw new CrowdRuntimeException("User ["+username+"] Not Found",e);
        } catch (OperationFailedException e) {
            throw new CrowdRuntimeException("Operation Failed Exception",e);
        } catch (ApplicationPermissionException e) {
            throw new CrowdRuntimeException("Application Permission Exception",e);
        } catch (InvalidAuthenticationException e) {
            throw new CrowdRuntimeException("Invalid Authentication",e);
        }
    }
}
