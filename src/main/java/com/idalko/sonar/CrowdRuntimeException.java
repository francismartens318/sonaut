package com.idalko.sonar;

/**
 * @author Hein Couwet
 */
@SuppressWarnings("serial")
public class CrowdRuntimeException extends RuntimeException {

    CrowdRuntimeException(String message) {
        super (message);
    }

    CrowdRuntimeException(String message,Throwable t) {
        super (message,t);
    }
}
