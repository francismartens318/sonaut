package com.idalko.sonar;

import com.atlassian.crowd.integration.rest.service.factory.RestCrowdClientFactory;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.client.ClientPropertiesImpl;
import com.atlassian.crowd.service.client.CrowdClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonar.api.config.Settings;
import org.sonar.api.security.ExternalGroupsProvider;
import org.sonar.api.security.ExternalUsersProvider;
import org.sonar.api.security.LoginPasswordAuthenticator;
import org.sonar.api.security.SecurityRealm;

import java.util.Properties;

/**
 * @author Hein Couwet
 */
public class CrowdRealm extends SecurityRealm {
    private static final Logger LOG = LoggerFactory.getLogger(CrowdRealm.class);

    private Settings settings;

    private LoginPasswordAuthenticator loginPasswordAuthenticator;
    private ExternalUsersProvider usersProvider;
    private ExternalGroupsProvider groupsProvider;


    public CrowdRealm(Settings settings) {
        this.settings = settings;
    }

    private String getSetting(String key, String defaultString) {
        String result = settings.getString(key);
        if (result == null) {
            result = defaultString;
            LOG.error("setting ["+key+"] is null, defaulting to ["+defaultString+"]");
        }
        return result;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void init() {

        String crowdUrl = getSetting("crowd.url", "http://localhost:8080");
        String applicationName = getSetting("crowd.application", "sonar");
        String applicationPassword = getSetting("crowd.password", "sonar");

        final Properties properties = new Properties();
        properties.setProperty("crowd.server.url", crowdUrl);
        properties.setProperty("application.name", applicationName);
        properties.setProperty("application.password", applicationPassword);
        properties.setProperty("session.validationinterval", "5");

        final ClientProperties clientProperties = ClientPropertiesImpl.newInstanceFromProperties(properties);
        final RestCrowdClientFactory restCrowdFactory = new com.atlassian.crowd.integration.rest.service.factory.RestCrowdClientFactory();
        final CrowdClient crowdClient = restCrowdFactory.newInstance(clientProperties);

        loginPasswordAuthenticator = new CrowdLoginPasswordAuthenticator(crowdClient);
        loginPasswordAuthenticator.init();
        usersProvider = new CrowdUsersProvider(crowdClient);
        groupsProvider = new CrowdGroupsProvider(crowdClient);

        LOG.info("Crowd Realm setup Done");
    }

    @Override
    public String getName() {
        return "Crowd";
    }

    @Override
    public LoginPasswordAuthenticator getLoginPasswordAuthenticator() {
        return loginPasswordAuthenticator;
    }

    @Override
    public ExternalUsersProvider getUsersProvider() {
         return usersProvider;
    }

    @Override
    public ExternalGroupsProvider getGroupsProvider() {
        return groupsProvider;
    }
}
