package com.idalko.sonar;

import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.service.client.CrowdClient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonar.api.security.LoginPasswordAuthenticator;



/**
 * @author Hein Couwet
 */

public class CrowdLoginPasswordAuthenticator implements LoginPasswordAuthenticator {
    private static final Logger LOG = LoggerFactory.getLogger(CrowdRealm.class);

    private CrowdClient crowdClient;

    CrowdLoginPasswordAuthenticator(CrowdClient crowdClient) {
        this.crowdClient=crowdClient;
        if (crowdClient == null) {
            throw new CrowdRuntimeException("crowdClient is null");
        }
    }


    @Override
    public void init() {
    }

    @Override
    public boolean authenticate(String username, String password) {
        try {
            crowdClient.authenticateUser(username, password);
            return true;
        } catch (UserNotFoundException e) {
            LOG.error("User ["+username+"]Not Found",e);
            return false;
        } catch (OperationFailedException e) {
            throw new CrowdRuntimeException("Operation Failed Exception",e);
        } catch (ApplicationPermissionException e) {
            throw new CrowdRuntimeException("Application Permission Exception",e);
        } catch (InvalidAuthenticationException e) {
            throw new CrowdRuntimeException("Invalid Authentication",e);
        } catch (InactiveAccountException e) {
            throw new CrowdRuntimeException("Inactive Account",e);
        } catch (ExpiredCredentialException e) {
            throw new CrowdRuntimeException("Expired Credential",e);
        }
    }
}
