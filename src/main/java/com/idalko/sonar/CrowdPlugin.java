package com.idalko.sonar;

import org.sonar.api.SonarPlugin;
import com.google.common.collect.ImmutableList;
import java.util.List;

/**
 * @author Hein Couwet
 */
public class CrowdPlugin extends SonarPlugin{

    @SuppressWarnings("rawtypes")
	@Override
    public List getExtensions() {
        return ImmutableList.of(CrowdRealm.class);
    }
}
