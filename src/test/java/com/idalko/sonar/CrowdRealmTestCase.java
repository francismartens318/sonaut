package com.idalko.sonar;

import org.junit.Test;
import org.sonar.api.config.Settings;

import static org.junit.Assert.*;

/**
 * @author Hein Couwet
 */
public class CrowdRealmTestCase {
    @Test
    public void testGetName() throws Exception {
        CrowdRealm realm = new CrowdRealm(null);
        assertEquals("Crowd",realm.getName());
    }

    @Test
    public void testInit() throws Exception {
        Settings settings = new Settings();
        settings.appendProperty("crowd.url","http://sonar.example.com");
        CrowdRealm realm = new CrowdRealm(settings);
        realm.init();
        assertNotNull(realm.getLoginPasswordAuthenticator());
        assertNotNull(realm.getUsersProvider());
        assertNotNull(realm.getGroupsProvider());
        assertEquals(CrowdLoginPasswordAuthenticator.class.getCanonicalName(),realm.getLoginPasswordAuthenticator().getClass().getCanonicalName());
        assertEquals(CrowdUsersProvider.class.getCanonicalName(),realm.getUsersProvider().getClass().getCanonicalName());
        assertEquals(CrowdGroupsProvider.class.getCanonicalName(),realm.getGroupsProvider().getClass().getCanonicalName());
    }


    @Test
    public void testWithoutInit() throws Exception {
        CrowdRealm realm = new CrowdRealm(null);
        assertNull(realm.getLoginPasswordAuthenticator());
        assertNull(realm.getGroupsProvider());
        assertNull(realm.getUsersProvider());
    }
}
