package com.idalko.sonar;

import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.service.client.CrowdClient;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * @author Hein Couwet
 */
public class CrowdLoginPasswordAuthenticatorTest {
    @Test
    public void testLoginPasswordAuthenticatorConstructor() throws Exception {
        try {
            CrowdClient crowdClient = null;
            new CrowdLoginPasswordAuthenticator(crowdClient);
            fail();
        } catch (RuntimeException e) {
            //Excepted failure;
        }
    }
    @Test
    public void testAuthenticateValidUser() throws Exception {
        CrowdClient crowdClient = mock(CrowdClient.class);
        User user = mock(User.class);
        when(crowdClient.authenticateUser("known","valid")).thenReturn(user);

        CrowdLoginPasswordAuthenticator loginPasswordAuthenticator = new CrowdLoginPasswordAuthenticator(crowdClient);
        assertTrue(loginPasswordAuthenticator.authenticate("known", "valid"));
    }

    @Test
    public void testAuthenticateInvalidUser() throws Exception {
        CrowdClient crowdClient = mock(CrowdClient.class);

        when(crowdClient.authenticateUser("known","invalid")).thenThrow(new UserNotFoundException("known"));

        CrowdLoginPasswordAuthenticator loginPasswordAuthenticator = new CrowdLoginPasswordAuthenticator(crowdClient);
        assertFalse(loginPasswordAuthenticator.authenticate("known", "invalid"));
    }

    @Test
    public void testAuthenticateExceptions() throws Exception {
        CrowdClient crowdClient = mock(CrowdClient.class);

        when(crowdClient.authenticateUser("failed", "")).thenThrow(new OperationFailedException());
        when(crowdClient.authenticateUser("permission", "")).thenThrow(new ApplicationPermissionException());
        when(crowdClient.authenticateUser("invalid", "")).thenThrow(new InvalidAuthenticationException("invalid"));
        when(crowdClient.authenticateUser("inactive", "")).thenThrow(new InactiveAccountException("inactive"));
        when(crowdClient.authenticateUser("expired", "")).thenThrow(new ExpiredCredentialException());


        CrowdLoginPasswordAuthenticator loginPasswordAuthenticator = new CrowdLoginPasswordAuthenticator(crowdClient);

        try {
            loginPasswordAuthenticator.authenticate("failed","");
            fail();
        } catch (RuntimeException e) {
            assertEquals("Operation Failed Exception",e.getMessage());
        }
        try {
            loginPasswordAuthenticator.authenticate("permission","");
            fail();
        } catch (RuntimeException e) {
            assertEquals("Application Permission Exception",e.getMessage());
        }
        try {
            loginPasswordAuthenticator.authenticate("invalid","");
            fail();
        } catch (RuntimeException e) {
            assertEquals("Invalid Authentication",e.getMessage());
        }
        try {
            loginPasswordAuthenticator.authenticate("inactive","");
            fail();
        } catch (RuntimeException e) {
            assertEquals("Inactive Account",e.getMessage());
        }

        try {
            loginPasswordAuthenticator.authenticate("expired","");
            fail();
        } catch (RuntimeException e) {
            assertEquals("Expired Credential",e.getMessage());
        }
    }

}
