package com.idalko.sonar;

import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.service.client.CrowdClient;
import org.sonar.api.security.UserDetails;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * @author Hein Couwet
 */
public class CrowdUsersProviderTestCase {
    @Test
    public void testDoGetUserDetailsConstructor() throws Exception {
        try {
            CrowdClient crowdClient = null;
            new CrowdUsersProvider(crowdClient);
            fail();
        } catch (RuntimeException e) {
            //Excepted failure;
        }
    }

    @Test
    public void testDoGetUserDetailsSuccess() throws Exception {
        CrowdClient crowdClient = mock(CrowdClient.class);
        User user = mock(User.class);
        when(crowdClient.getUser("nobody")).thenReturn(user);
        when(user.getDisplayName()).thenReturn("Mr Nobody");
        when(user.getEmailAddress()).thenReturn("mr@nobody.com");

        CrowdUsersProvider usersProvider = new CrowdUsersProvider(crowdClient);
        UserDetails userDetails = usersProvider.doGetUserDetails("nobody");
        assertEquals("Mr Nobody",userDetails.getName());
        assertEquals("mr@nobody.com",userDetails.getEmail());
    }

    @Test
    public void testDoGetUserDetailsFailures() throws Exception {
        CrowdClient crowdClient = mock(CrowdClient.class);

        when(crowdClient.getUser("unknown")).thenThrow(new UserNotFoundException("unknown"));
        when(crowdClient.getUser("failed")).thenThrow(new OperationFailedException());
        when(crowdClient.getUser("permission")).thenThrow(new ApplicationPermissionException());
        when(crowdClient.getUser("invalid")).thenThrow(new InvalidAuthenticationException("invalid"));

        CrowdUsersProvider usersProvider = new CrowdUsersProvider(crowdClient);

        try {
            usersProvider.doGetUserDetails("unknown");
            fail();
        } catch (RuntimeException e) {
            assertEquals("User [unknown] Not Found",e.getMessage());
        }
        try {
            usersProvider.doGetUserDetails("failed");
            fail();
        } catch (RuntimeException e) {
            assertEquals("Operation Failed Exception",e.getMessage());
        }
        try {
            usersProvider.doGetUserDetails("permission");
            fail();
        } catch (RuntimeException e) {
            assertEquals("Application Permission Exception",e.getMessage());
        }
        try {
            usersProvider.doGetUserDetails("invalid");
            fail();
        } catch (RuntimeException e) {
            assertEquals("Invalid Authentication",e.getMessage());
        }
    }
}
