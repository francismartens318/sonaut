package com.idalko.sonar;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Hein Couwet
 */
public class CrowdPluginTestCase {
    @Test
    public void testGetExtensions() throws Exception {
        CrowdPlugin plugin = new CrowdPlugin();
        @SuppressWarnings("rawtypes")
		List extensions =  plugin.getExtensions();
        assertNotNull(extensions);
        assertEquals(1,extensions.size());
        assertEquals(CrowdRealm.class,extensions.get(0));
    }
}
