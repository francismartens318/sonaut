package com.idalko.sonar;

import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.service.client.CrowdClient;

import com.google.common.collect.ImmutableList;


import java.util.Collection;
import java.util.List;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * @author Hein Couwet
 */
public class CrowdGroupsProviderTestCase {
    @Test
    public void testDoGetUserDetailsConstructor() throws Exception {
        try {
            CrowdClient crowdClient = null;
            new CrowdGroupsProvider(crowdClient);
            fail();
        } catch (RuntimeException e) {
            //Excepted failure;
        }
    }

    @Test
    public void testDoGetUserDetailsSuccess() throws Exception {
        CrowdClient crowdClient = mock(CrowdClient.class);
        List<String> groupCollection= ImmutableList.of("none","all");

        when(crowdClient.getNamesOfGroupsForNestedUser("nobody", 0, CrowdGroupsProvider.MAX_GROUPS)).thenReturn(groupCollection);

        CrowdGroupsProvider groupsProvider = new CrowdGroupsProvider(crowdClient);
        Collection<String> groups = groupsProvider.doGetGroups("nobody");
        assertNotNull(groups);
        assertEquals(2,groups.size());
        assertTrue(groups.contains("none"));
        assertTrue(groups.contains("all"));
    }

    @Test
    public void testDoGetUserDetailsFailures() throws Exception {
        CrowdClient crowdClient = mock(CrowdClient.class);
        when(crowdClient.getNamesOfGroupsForNestedUser("unknown", 0, CrowdGroupsProvider.MAX_GROUPS)).thenThrow(new UserNotFoundException("unknown"));
        when(crowdClient.getNamesOfGroupsForNestedUser("failed", 0, CrowdGroupsProvider.MAX_GROUPS)).thenThrow(new OperationFailedException());
        when(crowdClient.getNamesOfGroupsForNestedUser("permission", 0, CrowdGroupsProvider.MAX_GROUPS)).thenThrow(new ApplicationPermissionException());
        when(crowdClient.getNamesOfGroupsForNestedUser("invalid", 0, CrowdGroupsProvider.MAX_GROUPS)).thenThrow(new InvalidAuthenticationException("invalid"));

        CrowdGroupsProvider groupsProvider = new CrowdGroupsProvider(crowdClient);

        try {
            groupsProvider.doGetGroups("unknown");
            fail();
        } catch (RuntimeException e) {
            assertEquals("User [unknown] Not Found",e.getMessage());
        }
        try {
            groupsProvider.doGetGroups("failed");
            fail();
        } catch (RuntimeException e) {
            assertEquals("Operation Failed Exception",e.getMessage());
        }
        try {
            groupsProvider.doGetGroups("permission");
            fail();
        } catch (RuntimeException e) {
            assertEquals("Application Permission Exception",e.getMessage());
        }
        try {
            groupsProvider.doGetGroups("invalid");
            fail();
        } catch (RuntimeException e) {
            assertEquals("Invalid Authentication",e.getMessage());
        }

    }

}
