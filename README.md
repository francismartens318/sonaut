Use Case
========

For authentication and security you want to use JIRA or Crowd as a user server.

Installation
============

Place the jar file in the ${SONAR_HOME}/extensions/plugins directory

Configuration
=============

Jira
----
If you want to use jira as user server,
goto Administration > JIRA User Server
add an application with e.g.

+ application name : 'sonar'
+ password : 'sonar'

If the sonar server is not on the same server as the jira server, then

+ add the ip-address also of the sonar server.


Create a group sonar-administrators and sonar-users and make sure you assign users that need to use sonar.
The users only defined in sonar will not work anymore.


Sonar
-----

In order to use jira or crowd as the user server, add the following lines to the sonar.properties


    #url to the JIRA or crowd application
    crowd.url: http://localhost:8080

    #application name as defined in JIRA/crowd
    crowd.application: sonar

    #application password as defined in JIRA/crowd
    crowd.password: sonar

    #to enable this plugin put 'Crowd' in the following property
    sonar.security.realm: Crowd

    #sonar.authenticator.ignoreStartupFailure: true

    sonar.authenticator.createUsers: true

Restart sonar to activate the new configuration.
When a user will login, the plugin will authenticate against the JIRA/crowd application, and will

+ replace this user name/email with the Display name and email defined in JIRA/Crowd
+ replace the groups of that user with those defined in JIRA/Crowd and which already exists in Sonar.

Note:
  if you want to use another group than sonar-users and sonars-administrators, you need define these
  groups both in Crowd/JIRA and in Sonar
